const { shuffleList } = require("../src/utils/shuffling");

describe("Shuffling tests", () => {
  const list = [1, 2, 3, 4];

  it("should shuffle list", () => {
    const shuffledList = shuffleList(list);

    console.log(shuffledList);

    expect(shuffledList).not.toEqual(list);
  });
});
