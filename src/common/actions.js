const {
  KILL_ACTION,
  EXPOSE_ACTION,
  SAVE_ACTION,
  EXECUTE_ACTION,
  PLAYER_ALIVE,
  PLAYER_DEAD,
  PLAYER_EXPOSED
} = require("./constants");

const processActions = actions => actions.reduce((result, { action, target }) => {
  switch (action) {
    case KILL_ACTION:
      return {
        [target]: PLAYER_DEAD,
        ...result,
      };
    case EXPOSE_ACTION:
      return {
        [target]: result[target] !== PLAYER_DEAD ? PLAYER_EXPOSED : result[target]
      }
    case SAVE_ACTION:
      return {
        [target]: result[target] === PLAYER_DEAD ? PLAYER_ALIVE : result[target],
        ...result,
      };
    case EXECUTE_ACTION:
      return {
        [target]: PLAYER_DEAD,
        ...result,
      };
  }
}, {});

module.exports = { processActions };
