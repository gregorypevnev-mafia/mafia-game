const { DAY_STANDBY } = require("./common/constants");
const {
  phaseDetails,
  startingPhase,
  isStartingPhase,
  nextPhase
} = require("./common/phases");
const { processActions } = require("./common/actions");
const { countRoles } = require("./roles");

const STARTING_STEP = 1;

const initializeGame = ({ id, ...details }) => ({
  id,
  details,
  step: STARTING_STEP,
  phase: startingPhase(),
});

const next = ({ step, phase }) => {
  if (step === STARTING_STEP && isStartingPhase(phase))
    // Skipping trial on the first day
    return {
      step: STARTING_STEP,
      phase: DAY_STANDBY,
    };

  const { isFinalPhase, nextPhase: nextPhaseName } = nextPhase(phase);
  const nextStepNumber = isFinalPhase ? step + 1 : step;

  return {
    step: nextStepNumber,
    phase: nextPhaseName,
  };
};

const gameInfo = ({ id, details, step, phase }) => ({
  id,
  step,
  phase: phaseDetails(phase).title,
  ...details,
});

const phaseDuration = ({ phase }) => Number(phaseDetails(phase).duration);

// IMPORTANT: Separating Play-Phase and Standby-Phase for Play-Service
//  - NO Encapsulation
//  -> Allows determining whether passing Votes or Actions => Clearing Votes or Actions / Adding Action or 

const selectTarget = votes =>
  votes.reduce(({ max, counts }, { target }) => {
    counts[target] = (counts[target] || 0) + 1;

    return {
      max: counts[target] > counts[max] ? target : max,
      counts,
    }
  }, {
    max: null,
    counts: {},
  }).max;

// Returning an action
const processPlay = ({ phase }, votes) => {
  const phaseData = phaseDetails(phase);

  if (phaseData.standby) throw new Error("Not a Play Step");

  return {
    action: phaseData.action,
    target: selectTarget(votes),
  };
};

// Returning updated list of players
const processStandby = ({ phase }, players, actions) => {
  const phaseData = phaseDetails(phase);

  if (!phaseData.standby) throw new Error("Not a Standby Step");

  const results = processActions(actions);

  return players.map(player => {
    const newStatus = results[player.id];

    if (!newStatus) return player;

    return {
      ...player,
      status: newStatus,
    };
  });
};

const validateGame = ({ roles, players }) =>
  countRoles(roles) === players.length;

module.exports = {
  initializeGame,
  gameInfo,
  phaseDuration,
  next,
  processPlay,
  processStandby,
  validateGame,
};
